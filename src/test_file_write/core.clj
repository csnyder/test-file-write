(ns test-file-write.core
  (:gen-class)
  (:require [test-file-write.process :as pro] 
            [ras-db-library.core :as db]
            ))

 

(defn -main
  [& args]
  (db/connect)
  (let [fpath "tmp/out.xlsx"
        x (->> (pro/get-data pro/build-salsify pro/get-salsify)
               (pro/write-file fpath))]
    (if x
      "Success"
      "Fail")))
