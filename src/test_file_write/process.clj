(ns test-file-write.process
  (:require [clojure.string :as s]
            [test-file-write.data :as d]
            [clojure.data.json :as json]
            [dk.ative.docjure.spreadsheet :as excel]
            [ras-db-library.db.export :as db-export]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]))

;; --------------------------------------------------------
;; util.clj
;;---------------------------------------------------------
(defn check-empty-data  [data]
      (if  (empty? data) [] data))

(def salsify-template  "template/salsifytemplate.csv")

(defn get-salsify-template  []  
    (with-open  [reader  (io/reader salsify-template)]
      (doall
        (csv/read-csv reader))))
;;--------------------------------------------------------
;; salisfy.clj
;;---------------------------------------------------------

(defn format-view-data  [labels data-row]
  (map #(get data-row  (keyword  (s/lower-case %)))
              labels))

(defn build-salsify  [data] ;; header are coming out as keys 
    (clojure.pprint/pprint  "build-salsify")
      (let  [labels  (first  (get-salsify-template))]
            (concat  (vector labels) 
                                (map #(format-view-data labels %) data))))

(defn get-salsify  []  
    (clojure.pprint/pprint "get-data")
    (let  [payload  (db-export/get-salsify)]  
      ; come from the sql statment on ras-db-library 
      ;SELECT *  FROM exportvw_Salsifytest; 
      ;View is Complex and takes about 23 secs  
      ;to return with 3050 rows and 36 Columns
    (clojure.pprint/pprint "payload")
    (clojure.pprint/pprint "write-data-payload")
      (check-empty-data payload)))


;; --------------------------------------------------------
;; file.clj
;;---------------------------------------------------------

(defn file-exist  [file]
    (.exists  (io/file  (str file))))

(defn create-excel  [data filename]
    (clojure.pprint/pprint  "create-excel")
    (let  [wb  (excel/create-workbook "Export"
                                      data)]
          (clojure.pprint/pprint  "create-excel2")
              (if  (nil?  (excel/save-workbook! filename wb))
                    (file-exist filename))))

(defn get-data  [fn-build fn-data]
  (let  [payload  (fn-data) ]
    (fn-build payload)))

(defn write-file  [fpath data]
  (try 
    (clojure.pprint/pprint "write-file")
    (if  (not  (nil?  (io/make-parents fpath)))
      (create-excel data fpath)
      (throw  (Exception.  ""))) 
    (clojure.pprint/pprint "write-file2")
    (catch Exception e
      (println e)
      (throw  (Exception.  (str  "Problem Writing File : " fpath))))))


